window.onload = function() {

  var Point = function(x, y) {
    this.x = x;
    this.y = y;

    this.text_point = function() {
      return new Point(this.x + 10, this.y - 4);
    }
  }

  var Coord = function(lat, lon) {
    this.lat = lat;
    this.lon = lon;

    this.mapWidth = 2048;
    this.mapHeight = 1588;

    // pseudo-code was taken from
    // http://stackoverflow.com/questions/14329691/covert-latitude-longitude-point-to-a-pixels-x-y-on-mercator-projection
    this.to_geo = function() {
      var x = (this.lon+180)*(this.mapWidth/360);
      var latRad = this.lat * Math.PI/180;

      var mercN = Math.log(Math.tan((Math.PI/4)+(latRad/2)));
      var y = (this.mapHeight/2) - (this.mapWidth * mercN/(2*Math.PI));

      return new Point(x, y);
    }

  }

  var Map = function(canvas_id, map_id) {
    this.canvas = new fabric.StaticCanvas(canvas_id, {
      backgroundColor: 'rgb(200,200,255)',
      width: 2048,
      height: 1588
    });
    this.imgElement = document.getElementById(map_id);

    this.imgInstance = new fabric.Image(this.imgElement, {
      left: 0,
      top: 0,
    });

    this.draw = function() {
      this.canvas.add(this.imgInstance);
    }

    this.add_point = function(point, text) {
      var circle = new fabric.Circle({
        left: point.x,
        top: point.y,
        radius: 3,
        fill: '#f00'
      });

      var text = new fabric.Text(text, {
        fontFamily: 'Comic Sans',
        fontSize: 14,
        left: point.text_point().x,
        top: point.text_point().y,
        fill: '#f00'
      });

      this.canvas.add(circle);
      this.canvas.add(text);
    }; // add_point()

    this.add_city = function(coords, text) {
      mercator_coords = coords.to_geo();
      this.add_point(new Point(mercator_coords.x, mercator_coords.y), text);
    }
  };

  var map = new Map('c', 'map');
  map.draw();
  map.add_city(new Coord(51.5085300, -0.1257400),   'London');
  map.add_city(new Coord(40.7142700, -74.0059700),  'New York');
  map.add_city(new Coord(55.7522200, 37.6155600),   'Moskow');
  map.add_city(new Coord(-33.8678500, 151.2073200), 'Sydnay');
  map.add_city(new Coord(-34.6131500, -58.3772300), 'Buenos Aires');
}
